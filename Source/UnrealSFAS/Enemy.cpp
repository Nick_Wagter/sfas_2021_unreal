// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "MeleeWeapon.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);

	Health = 100;
	CanMove = true;
	IsAlive = true;
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	USkeletalMeshComponent* mesh = GetMesh();

	if (SwordObject)
	{
		FAttachmentTransformRules rules = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true);
		SwordActor = GetWorld()->SpawnActor<AActor>(SwordObject);
		SwordActor->AttachToComponent(mesh, rules, "hand_r_socket");
		Cast<AMeleeWeapon>(SwordActor)->Holder = this;
	}
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsAlive)
		return;

	if (AnimTime > 0)
	{
		AnimTime -= DeltaTime;
	}
	else if (!CanMove)
	{
		CanMove = true;
	}

	if (CooldownTime > 0 && CanMove)
	{
		CooldownTime -= DeltaTime;
	}
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AEnemy::OnAttack()
{
	if (!CanMove || CooldownTime > 0)
		return;

	//Play attack Animation	
	PlayAnimMontage(AttackAnimMontage);
	AnimTime = AttackAnimMontage->GetPlayLength();
	CooldownTime = AttackCooldown;
	CanMove = false;
}

void AEnemy::Damage(int Value)
{
	Health -= Value;

	if (Health <= 0 && IsAlive)
	{
		IsAlive = false;
		PlayAnimMontage(DeadAnimMontage);
		CanMove = false;
		SwordActor->Destroy();
	}

}

