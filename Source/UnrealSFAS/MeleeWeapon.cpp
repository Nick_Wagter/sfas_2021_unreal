// Fill out your copyright notice in the Description page of Project Settings.


#include "MeleeWeapon.h"
#include "IDamageable.h"

// Sets default values
AMeleeWeapon::AMeleeWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMeleeWeapon::BeginPlay()
{
	Super::BeginPlay();
	
	OnActorBeginOverlap.AddDynamic(this, &AMeleeWeapon::OnOverlap);

	UE_LOG(LogTemp, Warning, TEXT("Begin Weapon!"));
}



// Called every frame
void AMeleeWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
//================================================================
// Check if the Weapon has Overlapped with an Damageable
//================================================================
void AMeleeWeapon::OnOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (Holder == nullptr)
	{
		return;
	}

	if (OtherActor && (OtherActor != this) && OtherActor != Holder)
	{
		IIDamageable* actor = Cast<IIDamageable>(OtherActor);
		if (actor != nullptr)
		{
			actor->Damage(Damage);
		}
	}
}

