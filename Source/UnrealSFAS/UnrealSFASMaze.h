// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UnrealSFASMaze.generated.h"

UCLASS()
class UNREALSFAS_API AUnrealSFASMaze : public AActor
{
	GENERATED_BODY()
	

	enum class SpawnAbles : uint16
	{
		None = 0,
		Wall = 1,
		Door = 2,
		Chest_Key = 3,
		Chest_Potion = 4,
		Enemy_Melee = 5,
		PlayerSpawn = 6,
		PlayerExit = 7
	};

	enum class Corner
	{
		LeftTop,
		LeftBot,
		RightTop,
		RightBot
	};

public:	
	// Sets default values for this actor's properties
	AUnrealSFASMaze();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = Maze)
	void GenerateMaze(FString MazeFile, const int LevelId = 0);

private:

	void Generate(const TArray<uint16> Maze);

	void CheckPillarSpawn(const TArray<uint16> Maze, const uint16 X, const uint16 Y, USceneComponent* Root, const FTransform Trans);
	void SpawnWalls(const TArray<uint16> Maze, const uint16 X, const uint16 Y, USceneComponent* Root, const FTransform Trans);

	void SpawnWall(const FTransform Trans, USceneComponent* Root);
	void SpawnPillars(USceneComponent* Root, const FTransform Trans, const TArray<Corner> Corners);

public:

	UPROPERTY(EditDefaultsOnly, Category = Maze)
	UStaticMesh* WallMesh;
	UPROPERTY(EditDefaultsOnly, Category = Maze)
	UStaticMesh* PillarMesh;

	UPROPERTY(EditDefaultsOnly, Category = Maze_Spawnables)
	TSubclassOf<class AInteractable> DoorObject;

	UPROPERTY(EditDefaultsOnly, Category = Maze_Spawnables)
	TSubclassOf<class AInteractable> ChestObject;

	UPROPERTY(EditDefaultsOnly, Category = Maze_Spawnables)
	TSubclassOf<class AEnemy> MeleeEnemyObject;

	UPROPERTY(EditDefaultsOnly, Category = Maze_Spawnables)
	TSubclassOf<class AUnrealSFASCharacter> PlayerObject;

	UPROPERTY(EditDefaultsOnly, Category = Maze_Spawnables)
	TSubclassOf<class AExit> ExitObject;


	const float BlockSize = 300.0f;
	const float BlockWidth = 0.50f;
	const float BlockHeight = 0.75f;

	const float PillarScale = 1.0f;

	FVector2D CurrentMapSize;



};
