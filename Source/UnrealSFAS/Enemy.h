// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "IDamageable.h"

#include "Enemy.generated.h"

UCLASS()
class UNREALSFAS_API AEnemy : public ACharacter, public IIDamageable
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	void OnAttack();
	bool GetCanMove() { return CanMove; }
	bool GetIsAlive() { return IsAlive; }
	float GetAttackDistance() { return AttackDistance; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Damage(int Value) override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	AActor* SwordActor;
	float AnimTime;
	float CooldownTime;
	bool CanMove;
	bool IsAlive;

	UPROPERTY(EditAnywhere, Category = Stats)
		int Health = 100;
	UPROPERTY(EditAnywhere, Category = Stats)
		float AttackDistance = 150.0f;
	UPROPERTY(EditAnywhere, Category = Stats)
		float AttackCooldown = 1.5f;

	UPROPERTY(EditAnywhere, Category = Inventory)
		TSubclassOf<class AMeleeWeapon> SwordObject;

	UPROPERTY(EditAnywhere, Category = Animations)
		UAnimMontage* AttackAnimMontage;
	UPROPERTY(EditAnywhere, Category = Animations)
		UAnimMontage* DeadAnimMontage;

};
