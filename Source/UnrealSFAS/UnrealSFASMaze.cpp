// Fill out your copyright notice in the Description page of Project Settings.


#include "UnrealSFASMaze.h"

#include "UnrealSFASCharacter.h"
#include "Chest.h"
#include "Enemy.h"
#include "Exit.h"

// Sets default values
AUnrealSFASMaze::AUnrealSFASMaze()
{
 	// No need to tick the maze at this point
	PrimaryActorTick.bCanEverTick = false;

}

void AUnrealSFASMaze::BeginPlay()
{
	Super::BeginPlay();

}

//================================================================
// Call this when you need to Generate a Maze pass in a String
// of a Maze file to read out
//================================================================
void AUnrealSFASMaze::GenerateMaze(FString MazeFile, const int LevelId)
{
	TArray<uint16> maze;
	FVector2D size;
	bool canGenerate = false;

	if (!MazeFile.IsEmpty())
	{

	}
	
	if(!canGenerate)
	{
//================================================================
// Only Call this when there is no file available
// to generate dungeon with
//================================================================
		
		/*maze =
		{
			1,1,1,1,1,1,
			1,0,2,0,3,1,
			1,0,1,0,0,1,
			1,0,1,0,0,1,
			1,0,1,0,0,1,
			1,1,1,1,1,1,

		};
		size = FVector2D(6, 6);*/
	
		switch (LevelId)
		{
			case 0:
			{
				maze =
				{
					1,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,0,3,0,1,1,1,1,
					1,1,1,1,0,5,0,1,1,1,1,
					1,1,1,1,0,0,0,1,1,1,1,
					1,6,0,0,1,0,1,0,0,7,1,
					1,0,4,0,1,0,1,0,4,0,1,
					1,0,0,0,0,0,2,0,0,0,1,
					1,1,1,1,1,1,1,1,1,1,1,
				};

				size = FVector2D(11, 8);
				break;
			}
			case 1:
			{
				maze =
				{
					1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,1,
					1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,3,1,
					1,1,1,1,0,0,1,1,1,1,1,1,4,5,1,1,1,1,1,1,5,1,1,1,1,
					1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,0,0,0,2,0,0,0,0,7,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				};

				size = FVector2D(25, 10);
				break;
			}
			case 2:
			{
				maze =
				{
					1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,6,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,0,4,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,0,0,0,0,0,0,0,0,0,5,4,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,0,0,0,0,0,0,0,0,0,3,0,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,7,1,1,1,1,
					1,1,1,1,1,1,1,1,1,1,1,1,1,1,4,5,1,1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				};

				size = FVector2D(25, 11);
				break;
			}
		}
	}
	CurrentMapSize = size;

	Generate(maze);
}


/*switch (LevelId)
{
			case 0:
			{
				maze =
				{
					1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,0,0,0,0,4,3,0,0,0,0,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,0,5,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,
					1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,5,0,0,1,1,1,1,1,1,1,
					1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,
					1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,
					1,0,0,0,0,0,0,1,1,1,1,1,0,0,1,1,1,1,0,0,0,0,0,0,1,
					1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,1,
					1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,
					1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,1,
					1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,7,1,
					1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,1,
					1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,1,
					1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				};

				size = FVector2D(25, 15);
				break;
			}
			case 1:
			{

			maze =
			{
				1,1,1,1,1,1,1,1,1,1,1,
				1,1,1,1,3,0,3,1,1,1,1,
				1,1,1,1,0,3,0,1,1,1,1,
				1,1,1,1,0,0,0,1,1,1,1,
				1,0,0,5,1,0,1,0,0,7,1,
				1,0,4,0,1,0,1,0,3,0,1,
				1,0,0,0,2,0,2,0,0,0,1,
				1,1,1,1,1,1,1,1,1,1,1,
			};

			size = FVector2D(11, 8);
				break;
			}
			case 2:
			{
				break;
			}
}
*/

//================================================================
// Generates the Geometry and spawns the Objects as they
// are assigned in the map
//================================================================
void AUnrealSFASMaze::Generate(TArray<uint16> Maze)
{
	if (WallMesh)
	{

		float xPos = 0.0f;
		float yPos = 0.0f;
		FQuat worldRotation(FVector(0.0f, 0.0f, 1.0f), 0.0f);
		FVector worldScale(BlockWidth, BlockWidth, BlockHeight);

		USceneComponent* rootComponent = GetRootComponent();

		// Loop through the binary values to generate the maze as static mesh components attached to the root of this actor
		for (uint16 x = 0; x < CurrentMapSize.X; x++)
		{
			xPos = static_cast<float>(x - (CurrentMapSize.X / 2)) * BlockSize;
			for (uint16 y = 0; y < CurrentMapSize.Y; y++)
			{
				yPos = static_cast<float>(y - (CurrentMapSize.Y / 2)) * BlockSize;
				SpawnAbles spawnable = SpawnAbles(Maze[(y * CurrentMapSize.X) + x]);

				FVector worldPosition(xPos - 5, yPos, 0);
				FTransform worldXForm(worldRotation, worldPosition, worldScale);

				if (spawnable != SpawnAbles::Wall)
				{
					CheckPillarSpawn(Maze, x, y, rootComponent, worldXForm);
				}

				switch (spawnable)
				{
					case SpawnAbles::None:
					{
						continue;
					}
					case SpawnAbles::Wall:
					{
						SpawnWalls(Maze, x, y, rootComponent, worldXForm);
						break;
					}
					case SpawnAbles::Door:
					{
						FActorSpawnParameters spawnParams;
						spawnParams.Owner = this;

						auto door = GetWorld()->SpawnActor<AInteractable>(DoorObject, worldXForm, spawnParams);

						worldPosition.X -= (BlockSize / 2);
						worldPosition.Y += (BlockSize);

						door->SetActorLocation(worldPosition);
						door->SetActorScale3D(FVector(1.5f,1.5,1.3f));
						break;
					}
					case SpawnAbles::Chest_Key:
					case SpawnAbles::Chest_Potion:
					{

						FActorSpawnParameters spawnParams;
						spawnParams.Owner = this;
						AChest* chest = Cast<AChest>(GetWorld()->SpawnActor<AInteractable>(ChestObject, worldXForm, spawnParams));

						worldPosition.X -= (BlockSize / 2);
						worldPosition.Y += (BlockSize / 2);

						chest->SetActorLocation(worldPosition);

						if (spawnable == SpawnAbles::Chest_Key)
						{

							chest->SetItem(1);

						}
						else if (spawnable == SpawnAbles::Chest_Potion)
						{
							chest->SetItem(2);

						}
						break;
					}
					case SpawnAbles::Enemy_Melee:
					{
						FActorSpawnParameters spawnParams;
						auto a = GetWorld()->SpawnActor<AEnemy>(MeleeEnemyObject, worldXForm, spawnParams);

						worldPosition.X -= (BlockSize / 2);
						worldPosition.Y += (BlockSize / 2);
						worldPosition.Z += (BlockSize / 2);

						a->SetActorLocation(worldPosition);
						a->SetActorScale3D(FVector(1.f, 1.f, 1.f));
						break;
					}
					case SpawnAbles::PlayerSpawn:
					{
						FActorSpawnParameters spawnParams;
						auto a = GetWorld()->SpawnActor<AUnrealSFASCharacter>(PlayerObject, worldXForm, spawnParams);

						worldPosition.X -= (BlockSize / 2);
						worldPosition.Y += (BlockSize / 2);
						worldPosition.Z += (BlockSize / 2);

						a->SetActorLocation(worldPosition);
						a->SetActorScale3D(FVector(1.f, 1.f, 1.f));
						break;
					}					
					case SpawnAbles::PlayerExit:
					{
						FActorSpawnParameters spawnParams;
						auto a = GetWorld()->SpawnActor<AExit>(ExitObject, worldXForm, spawnParams);

						worldPosition.X -= (BlockSize / 2);
						worldPosition.Y += (BlockSize / 2);

						a->SetActorLocation(worldPosition);
						a->SetActorScale3D(FVector(1.f, 1.f, 1.f));
						break;
					}
				}
			}
		}

	}
}
void AUnrealSFASMaze::CheckPillarSpawn(const TArray<uint16> Maze, const uint16 X, const uint16 Y, USceneComponent* Root, const FTransform Trans)
{
	TArray<Corner> corners;

	FVector2D l, r, t, b;

	l = FVector2D(X - 1, Y);
	r = FVector2D(X + 1, Y );
	t = FVector2D(X, Y - 1);
	b = FVector2D(X, Y + 1);

	if ((X - 1) >= 0 && X < CurrentMapSize.X - 1)
	{
		if ((Y - 1) >= 0 && Y < CurrentMapSize.Y - 1)
		{
			if (SpawnAbles(Maze[(l.Y * CurrentMapSize.X) + l.X]) == SpawnAbles::Wall && SpawnAbles(Maze[(t.Y * CurrentMapSize.X) + t.X]) == SpawnAbles::Wall)
				corners.Add(Corner::LeftTop);
			if (SpawnAbles(Maze[(l.Y * CurrentMapSize.X) + l.X]) == SpawnAbles::Wall && SpawnAbles(Maze[(b.Y * CurrentMapSize.X) + b.X]) == SpawnAbles::Wall)
				corners.Add(Corner::LeftBot);
			if (SpawnAbles(Maze[(r.Y * CurrentMapSize.X) + r.X]) == SpawnAbles::Wall && SpawnAbles(Maze[(t.Y * CurrentMapSize.X) + t.X]) == SpawnAbles::Wall)
				corners.Add(Corner::RightTop);
			if (SpawnAbles(Maze[(r.Y * CurrentMapSize.X) + r.X]) == SpawnAbles::Wall && SpawnAbles(Maze[(b.Y * CurrentMapSize.X) + b.X]) == SpawnAbles::Wall)
				corners.Add(Corner::RightBot);

			if (SpawnAbles(Maze[(t.Y * CurrentMapSize.X) + l.X]) == SpawnAbles::Wall) corners.Add(Corner::LeftTop);
			if (SpawnAbles(Maze[(t.Y * CurrentMapSize.X) + r.X]) == SpawnAbles::Wall) corners.Add(Corner::RightTop);
			if(SpawnAbles(Maze[(b.Y * CurrentMapSize.X) + l.X]) == SpawnAbles::Wall) corners.Add(Corner::LeftBot);
			if (SpawnAbles(Maze[(b.Y * CurrentMapSize.X) + r.X]) == SpawnAbles::Wall) corners.Add(Corner::RightBot);
		}
	}

	SpawnPillars(Root, Trans, corners);
}

void AUnrealSFASMaze::SpawnWalls(const TArray<uint16> Maze, const uint16 X, const uint16 Y, USceneComponent* Root, const FTransform Trans)
{
	//Which Sides determination and spawn on location required
	FTransform trans = Trans;

	FRotator NewRotation = FRotator(0, 0, 0);
	FQuat rot = FQuat(NewRotation);
	FVector location = trans.GetLocation();

	int x = (X - 1);
	if (x >= 0)
	{
		if (SpawnAbles(Maze[(Y * CurrentMapSize.X) + x]) != SpawnAbles::Wall)
		{
			trans.SetRotation(rot);
			trans.SetLocation(location + FVector((-BlockSize), 0, 0));

			SpawnWall(trans, Root);
			trans = Trans;
		}
	}
	if (X < CurrentMapSize.X - 1)
	{
		if (SpawnAbles(Maze[(Y * CurrentMapSize.X) + (X + 1)]) != SpawnAbles::Wall)
		{
			trans.SetRotation(rot);
			trans.SetLocation(location + FVector(0, 0, 0));

			SpawnWall(trans, Root);
			trans = Trans;
		}
	}

	NewRotation.Yaw = 90;
	rot = FQuat(NewRotation);
	trans.SetRotation(rot);

	int y = (Y - 1);
	if (y >= 0)
	{
		if (SpawnAbles(Maze[(y * CurrentMapSize.X) + X]) != SpawnAbles::Wall)
		{
			FVector offset = FVector(0, 0, 0);
			trans.SetLocation(location + offset);
			SpawnWall(trans, Root);
			trans = Trans;
		}
	}
	if (Y < CurrentMapSize.Y - 1)
	{

		if (SpawnAbles(Maze[((Y + 1) * CurrentMapSize.X) + X]) != SpawnAbles::Wall)
		{
			FVector offset = FVector(0, BlockSize, 0);
			trans.SetLocation(location + offset);
			SpawnWall(trans, Root);
			trans = Trans;
		}
	}
}

void AUnrealSFASMaze::SpawnWall(const FTransform Trans, USceneComponent* Root)
{
	UStaticMeshComponent* meshComponent = NewObject<UStaticMeshComponent>(this);

	meshComponent->SetStaticMesh(WallMesh);
	meshComponent->SetWorldTransform(Trans);
	meshComponent->AttachToComponent(Root, FAttachmentTransformRules::KeepWorldTransform);
	meshComponent->RegisterComponent();
}

void AUnrealSFASMaze::SpawnPillars(USceneComponent* Root, const FTransform Trans, const TArray<Corner> Corners)
{
	FTransform trans = Trans;
	trans.SetScale3D(FVector(PillarScale));

	for (auto c : Corners)
	{
		FVector location = trans.GetLocation();

		switch (c)
		{
			case Corner::LeftTop:
			{
				location += FVector(-BlockSize, 0, 0);
				break;
			}
			case Corner::LeftBot:
			{
				location += FVector(-BlockSize, BlockSize, 0);
				break;
			}
			case Corner::RightTop:
			{
				location += FVector(0, 0, 0);
				break;
			}
			case Corner::RightBot:
			{
				location += FVector(0, BlockSize, 0);
				break;
			}
		}

		UStaticMeshComponent* meshComponent = NewObject<UStaticMeshComponent>(this);
		trans.SetLocation(location);
		meshComponent->SetStaticMesh(PillarMesh);
		meshComponent->SetWorldTransform(trans);
		meshComponent->AttachToComponent(Root, FAttachmentTransformRules::KeepWorldTransform);
		meshComponent->RegisterComponent();

		trans.SetLocation(Trans.GetLocation());
	}
}