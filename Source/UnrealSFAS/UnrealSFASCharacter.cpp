// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealSFASCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

#include "Interactable.h"
#include "MeleeWeapon.h"
#include "Exit.h"

//////////////////////////////////////////////////////////////////////////
// AUnrealSFASCharacter

AUnrealSFASCharacter::AUnrealSFASCharacter()
{
	// Set size for collision capsule
	UCapsuleComponent* capsule = GetCapsuleComponent();
	capsule->InitCapsuleSize(42.f, 96.0f);

	Interactable = nullptr;

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->MaxWalkSpeed = StartSpeed;
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	//Set up Inital Data
	Health = MaxHealth;
	Keys = 0;
	Potions = 0;
	IsSprinting = false;
	AnimTime = 0;
	CanMove = true;
	IsAlive = true;
}

// Called when the game starts or when spawned
void AUnrealSFASCharacter::BeginPlay()
{
	Super::BeginPlay();

	UCapsuleComponent* capsule = GetCapsuleComponent();

	// declare overlap events
	capsule->OnComponentBeginOverlap.AddDynamic(this, &AUnrealSFASCharacter::OnOverlap);
	capsule->OnComponentEndOverlap.AddDynamic(this, &AUnrealSFASCharacter::OnOverlapEnd);
	//Spawn Sword and Shield
	USkeletalMeshComponent* mesh = GetMesh();

	if (SwordObject)
	{
		FAttachmentTransformRules rules = FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true);
		SwordActor = GetWorld()->SpawnActor<AActor>(SwordObject);
		SwordActor->AttachToComponent(mesh, rules, "hand_r_socket");
		Cast<AMeleeWeapon>(SwordActor)->Holder = this;
	}

	OnUpdateUI();
}


// Called every frame
void AUnrealSFASCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsAlive)
		return;

	if (AnimTime > 0)
	{
		AnimTime -= DeltaTime;
	}
	else if (!CanMove)
	{
		CanMove = true;
	}

}

//////////////////////////////////////////////////////////////////////////
// Input

void AUnrealSFASCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &AUnrealSFASCharacter::Attack);
	PlayerInputComponent->BindAction("UseItem", IE_Pressed, this, &AUnrealSFASCharacter::UseItem);
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AUnrealSFASCharacter::Interact);
	
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AUnrealSFASCharacter::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AUnrealSFASCharacter::StopSprint);


	PlayerInputComponent->BindAxis("MoveForward", this, &AUnrealSFASCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AUnrealSFASCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AUnrealSFASCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AUnrealSFASCharacter::LookUpAtRate);

}


//================================================================
// Check if the Player has Overlapped with an Object
//================================================================
void AUnrealSFASCharacter::OnOverlap(class UPrimitiveComponent* OverlappedComp,
	class AActor* OtherActor,
	class UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		AInteractable* interact = Cast<AInteractable>(OtherActor);
		if (interact != nullptr && interact->CanInteract())
		{
			Interactable = interact;
			OnUpdateUI();
		}
		AExit* exit = Cast<AExit>(OtherActor);
		if (exit != nullptr)
		{
			exit->OnExit();
		}
	}
}
//================================================================
// Check if the Player has Ended overlapping with an Object
//================================================================
void AUnrealSFASCharacter::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp,
	class AActor* OtherActor,
	class UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
	if (Interactable == nullptr)
	{
		return;
	}

	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		if (OtherActor == Interactable)
		{
			Interactable = nullptr;
			OnUpdateUI();
		}
	}
}

void AUnrealSFASCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AUnrealSFASCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AUnrealSFASCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && CanMove)
	{
		float v = Value;

		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, v);
	}
}

void AUnrealSFASCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) && CanMove)
	{
		float v = Value;

		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, v);
	}
}

void AUnrealSFASCharacter::Attack()
{
	if (!CanMove)
		return;

	//Play attack Animation	
	PlayAnimMontage(AttackAnimMontage);
	AnimTime = AttackAnimMontage->GetPlayLength();
	CanMove = false;
}

void AUnrealSFASCharacter::Interact()
{
	if (Interactable != nullptr)
	{
		if (Interactable->Interact(this))
		{
			Interactable = nullptr;
			OnUpdateUI();
		}
	}
}

void AUnrealSFASCharacter::Sprint()
{
	if (!IsSprinting)
	{
		IsSprinting = true;
		GetCharacterMovement()->MaxWalkSpeed = StartSpeed + (StartSpeed * SprintMultiplier);
	}
}

void AUnrealSFASCharacter::StopSprint()
{
	IsSprinting = false;
	GetCharacterMovement()->MaxWalkSpeed = StartSpeed;
}

void AUnrealSFASCharacter::UseItem()
{
	if (Potions > 0 && Health < MaxHealth)
	{
		Potions--;
		Health += ((MaxHealth / 100) * 20);
		OnUpdateUI();
	}
}

bool AUnrealSFASCharacter::UseKey()
{
	if (Keys <= 0)
	{
		return false;
	}

	Keys--;

	return true;
}
void AUnrealSFASCharacter::Damage(int Value)
{
	Health -= Value;
	OnUpdateUI();

	if (Health <= 0 && IsAlive)
	{
		IsAlive = false;
		PlayAnimMontage(DeadAnimMontage);
		CanMove = false;
		SwordActor->Destroy();
		OnDeath();
	}
}