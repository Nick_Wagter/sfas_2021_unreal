// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyController.h"

#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Kismet/KismetMathLibrary.h"

#include "UnrealSFASCharacter.h"
#include "Enemy.h"

AEnemyController::AEnemyController()
{
	PrimaryActorTick.bCanEverTick = true;

	SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));

	SightConfig->SightRadius = AISightRadius;
	SightConfig->LoseSightRadius = AILoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = AIFieldOfView;
	SightConfig->SetMaxAge(AISightAge);

	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;

	GetPerceptionComponent()->SetDominantSense(*SightConfig->GetSenseImplementation());
	GetPerceptionComponent()->ConfigureSense(*SightConfig);
}

void AEnemyController::BeginPlay()
{
	Super::BeginPlay();

	ControlledEnemy = Cast<AEnemy>(GetPawn());

	if (GetPerceptionComponent() == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("AI Controller Problem | NOT SET!"));
	}
}

void AEnemyController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);


	if (ControlledEnemy == nullptr)
	{
		ControlledEnemy = Cast<AEnemy>(GetPawn());
		return;
	}
	if (!ControlledEnemy->GetIsAlive())
	{
		return;
	}

	AActor* target = DetectActors();

	if (!ControlledEnemy->GetCanMove() || target == nullptr)
	{
		FRotator rot = GetControlRotation();
		rot.Yaw += RotationRate * DeltaSeconds;
		FQuat newQuad = rot.Quaternion();
		ControlledEnemy->SetActorRotation(newQuad);

		MoveToActor(ControlledEnemy, (ControlledEnemy->GetAttackDistance() / 4));
		return;
	}
	float distance = FVector::Distance(target->GetTransform().GetLocation(), ControlledEnemy->GetTransform().GetLocation());

	if (ControlledEnemy->GetAttackDistance() < distance)
	{
		MoveToActor(target, (ControlledEnemy->GetAttackDistance() / 4));
	}
	else
	{
		FRotator Lookat = UKismetMathLibrary::FindLookAtRotation(ControlledEnemy->GetActorLocation(), target->GetActorLocation());
		ControlledEnemy->SetActorRotation(Lookat.Quaternion());
		ControlledEnemy->OnAttack();
	}
}

FRotator AEnemyController::GetControlRotation() const
{
	if (GetPawn() == nullptr)
	{
		return FRotator(0.0f, 0.0f, 0.0f);
	}

	return FRotator(0.0f, GetPawn()->GetActorRotation().Yaw, 0.0f);
}

void AEnemyController::OnPossess(APawn* PossesedPawn)
{
	Super::OnPossess(PossesedPawn);
}

AActor* AEnemyController::DetectActors()
{
	TArray<AActor*> sensedActors;
	GetPerceptionComponent()->GetCurrentlyPerceivedActors(nullptr, sensedActors);

	AActor* player = nullptr;
	bool playerFound = false;

	for (auto a : sensedActors)
	{
		AUnrealSFASCharacter* pChar = Cast<AUnrealSFASCharacter>(a);
		if (pChar != nullptr && pChar->GetIsAlive())
		{
			player = a;
			break;
		}
	}

	return player;
}