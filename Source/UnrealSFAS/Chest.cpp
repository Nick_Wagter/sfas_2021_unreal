// Fill out your copyright notice in the Description page of Project Settings.


#include "Chest.h"

#include "UnrealSFASCharacter.h"

bool AChest::Interact(AUnrealSFASCharacter* Player)
{
	if (IsInteractable)
	{
		IsInteractable = false;

		if (Item == 1)
		{
			Player->AddKey();
		}
		else if(Item == 2)
		{
			Player->AddPotion();
		}

		this->Destroy();
		return true;
	}

	return false;
}

void AChest::ResetInteractable()
{
	Super::ResetInteractable();
	UE_LOG(LogTemp, Warning, TEXT("Chest Reset!"));
	InteractableName = "Chest";
}