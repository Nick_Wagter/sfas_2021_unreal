// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.generated.h"

class AUnrealSFASCharacter;

UCLASS()
class UNREALSFAS_API AInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Interactable)
	bool IsInteractable;
	FString InteractableName;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual bool Interact(AUnrealSFASCharacter* Player);
	virtual void ResetInteractable();

	bool CanInteract() { return IsInteractable; }
	UFUNCTION(BlueprintCallable, Category = "Interactable")
	FString GetName() { return InteractableName; }
};
