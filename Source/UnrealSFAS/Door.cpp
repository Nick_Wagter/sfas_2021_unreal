// Fill out your copyright notice in the Description page of Project Settings.


#include "Door.h"

#include "UnrealSFASCharacter.h"

bool ADoor::Interact(AUnrealSFASCharacter* Player)
{
	if (Player->UseKey())
	{
		IsInteractable = false;

		return true;
	}

	return false;
}

void ADoor::ResetInteractable()
{
	Super::ResetInteractable();
	InteractableName = "Door";
}