// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "IDamageable.h"

#include "UnrealSFASCharacter.generated.h"


UCLASS(config=Game)
class AUnrealSFASCharacter : public ACharacter, public IIDamageable
{
	GENERATED_BODY()

public:
	AUnrealSFASCharacter();


	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }


	virtual void Damage(int Value) override;

	void AddKey() { Keys++; }
	bool UseKey();
	void AddPotion() { Potions++; }
	bool GetIsAlive() { return IsAlive; }
	UFUNCTION(BlueprintCallable, Category = "Stats")
	int GetMaxHealth() { return MaxHealth; }

	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void OnUpdateUI();

	UFUNCTION(BlueprintImplementableEvent, Category = "Stats")
	void OnDeath();

protected:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnOverlap(class UPrimitiveComponent* OverlappedComp,
		class AActor* OtherActor,
		class UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp,
		class AActor* OtherActor,
		class UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex);

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	void Attack();
	void Interact();

	void Sprint();
	void StopSprint();
	void UseItem();

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface



	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Interactable)
		class AInteractable* Interactable;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
		bool IsSprinting;

	/** Inventory. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Inventory)
		int Keys;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Inventory)
		int Potions;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Stats")
		int MaxHealth = 100;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Stats")
		int Health = 100;

private:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

	AActor* SwordActor;

	UPROPERTY(EditAnywhere, Category = Movement)
		float StartSpeed = 600.0f;
	UPROPERTY(EditAnywhere, Category = Movement)
		float SprintMultiplier = 0.75f;

	UPROPERTY(EditAnywhere, Category = Inventory)
		TSubclassOf<class AMeleeWeapon> SwordObject;

	/* Animations */
	UPROPERTY(EditAnywhere, Category = Animations)
		UAnimMontage* AttackAnimMontage;
	UPROPERTY(EditAnywhere, Category = Animations)
		UAnimMontage* DeadAnimMontage;

	float AnimTime;
	bool CanMove;
	bool IsAlive;
};

