// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "Chest.generated.h"

/**
 * 
 */

UCLASS()
class UNREALSFAS_API AChest : public AInteractable
{
	GENERATED_BODY()

public:
	virtual bool Interact(AUnrealSFASCharacter* Player) override;
	virtual void ResetInteractable() override;

	void SetItem(int ItemId) { Item = ItemId; }
private:
	UPROPERTY(EditAnywhere, Category = Item)
	int Item;
};
